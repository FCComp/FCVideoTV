//
//  VideoTV001TVDatailtsPageVC.h
//  DDMyFramework
//
//  Created by omni－appple on 2019/7/17.
//  Copyright © 2019年 COM.Sobey.dengjie. All rights reserved.
//

#import <TMSDK/TMSDK.h>
#import <FCBaseKit/FCBaseKit.h>
#import "VideoTV001TVListModel.h"
@interface VideoTV001TVDatailtsPageVC : FCViewController
/**
 0  视频  1 广播
 */
@property (nonatomic , assign) NSInteger type;
/// 0无聊天室有节目单  1  有聊天室  3 都不需要
@property (nonatomic , assign) NSInteger style;
@property (nonatomic , assign) NSInteger tv_id;
@property (nonatomic , assign) NSInteger radio_id;
@property (nonatomic , copy) NSString * component_id;

 
@property (nonatomic, strong) VideoTV001TVListModel *model;
@property (nonatomic, strong) VideoTV001TVListModelList * listModel;

@end

@interface VideoTV001TVDatailtsRadioPageVC : VideoTV001TVDatailtsPageVC

@end
