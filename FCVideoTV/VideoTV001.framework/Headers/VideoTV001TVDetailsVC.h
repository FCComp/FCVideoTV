//
//  VideoTV001TVDetailsVC.h
//  DDMyFramework
//
//  Created by omni－appple on 2019/7/17.
//  Copyright © 2019年 COM.Sobey.dengjie. All rights reserved.
//  VideoTV001TVDetailsViewModel  VideoTV001TVDetailTopView 

#import <TMSDK/TMSDK.h>
#import <FCBaseKit/FCBaseKit.h>
@interface VideoTV001TVDetailsVC : FCViewController
///  重置 回到今天日期
- (void)resetTableLoadConfig;
/// 正在播放id
@property (nonatomic ,assign) NSInteger isPlay_id;
@property (nonatomic , assign) NSInteger radio_id;
@property (nonatomic , assign) NSInteger tv_id;
// 刷新右边table
- (void)reloadData;

@end
