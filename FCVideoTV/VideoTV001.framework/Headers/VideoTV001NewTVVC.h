//
//  VideoTV001NewTVVC.h
//  DDMyFramework
//
//  Created by omni－appple on 2019/7/15.
//  Copyright © 2019年 COM.Sobey.dengjie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TMSDK/TMSDK.h>
#import <FCBaseKit/FCBaseKit.h>
@interface VideoTV001NewTVVC : FCViewController

/**
  	0看电视 1 广播
*/
@property (nonatomic , assign) NSInteger type;
@end
